
// ImageCompressionJPEG.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'pch.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols


// CImageCompressionJPEGApp:
// See ImageCompressionJPEG.cpp for the implementation of this class
//

class CImageCompressionJPEGApp : public CWinApp
{
public:
	CImageCompressionJPEGApp();

// Overrides
public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CImageCompressionJPEGApp theApp;
