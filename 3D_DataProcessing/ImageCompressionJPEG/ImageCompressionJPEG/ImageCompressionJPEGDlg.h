
// ImageCompressionJPEGDlg.h : header file
//

#pragma once
#include <vector>
#include "GDIClass.h"
#include <gl/GL.h>
#include <complex>
#include <fstream>
#include <stdio.h>
using namespace std;

// CImageCompressionJPEGDlg dialog
class CImageCompressionJPEGDlg : public CDialogEx
{
	// Construction
public:
	CImageCompressionJPEGDlg(CWnd* pParent = nullptr);	// standard constructor

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_IMAGECOMPRESSIONJPEG_DIALOG };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedSearch();
	afx_msg void OnBnClickedCompress();
	vector<vector<float>> picture;
	vector<vector<complex<float>>> spectrumOfPicture;
	vector<vector<complex<float>>> spectrumOfBlock;
	vector<vector<complex<float>>> comperessedCoef;
	vector<vector<float>> restoredPicture;
	const int headerSizeBmp = 54;
	int width, height;
	int sizeImgRaw;
	int sizeCompressImg;
	int blockCount;
	const int blockSize = 8;
	int compressType; // might be 6, 12 or 16 (equals count of saved coeffs)
	void Fourea(vector <complex<float>>& data, int is);
	void Fourea_2D(vector<vector<complex<float>>>& data, int is);
	void Transpose_2Dmatrix(vector<vector<complex<float>>> input, vector<vector<complex<float>>>& output);
	void Transpose_2Dmatrix(vector<vector<float>> input, vector<vector<float>>& output);
	void GetBlockFromImg(vector<vector<float>> input, vector<vector<float>>& block, int ptr_x, int ptr_y);
	int Save12Coefficients();
	int OutputVectorInFile(vector<vector<float>> v, string s);
	int Save24Coefficients();
	int Save48Coefficients();
	int GetSizeNonZeroValueInVector(vector<vector<complex<float>>> v);
	double CalcStandardDeviation(vector<vector<float>> v1, vector<vector<float>> v2);
	inline void resizeVector(vector<vector<complex<float>>> &vvvc, int size) {
		vvvc.resize(size);
		for (int i = 0; i < size; i++)
		{
			vvvc[i].resize(size);
		}
	}

	inline void resizeVector(vector<vector<float>> &vv, int size)
	{
		vv.resize(size);
		for (int i = 0; i < size; i++)
			vv[i].resize(size);
	}
	Bitmap* image;
	GDIClass gc1;
	CString image_path;
	GDIClass gc2;
	CSliderCtrl coeffCompress;
	// ration between sizes of input image and compressed image
	double ratio;

	double deviation;
};
