// ImageCompressionJPEGDlg.cpp : implementation file
//

#include "pch.h"
#include "framework.h"
#include "ImageCompressionJPEG.h"
#include "ImageCompressionJPEGDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CImageCompressionJPEGDlg dialog



CImageCompressionJPEGDlg::CImageCompressionJPEGDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_IMAGECOMPRESSIONJPEG_DIALOG, pParent)
	, image_path(_T(""))
	, sizeImgRaw(0)
	, sizeCompressImg(0)
	, ratio(0)
	, deviation(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CImageCompressionJPEGDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_INPUT_PIC, gc1);
	DDX_Text(pDX, IDC_PATH_PIC, image_path);
	DDX_Text(pDX, IDC_SIZE, sizeImgRaw);
	DDX_Control(pDX, IDC_SPECTRUM, gc2);
	DDX_Control(pDX, IDC_COEFF_COMPRESS, coeffCompress);
	DDX_Text(pDX, IDC_SIZE_COMPRESS_IMG, sizeCompressImg);
	DDX_Text(pDX, IDC_RATIO, ratio);
	DDX_Text(pDX, IDC_DEVIATION, deviation);
}

BEGIN_MESSAGE_MAP(CImageCompressionJPEGDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_SEARCH, &CImageCompressionJPEGDlg::OnBnClickedSearch)
	ON_BN_CLICKED(IDC_COMPRESS, &CImageCompressionJPEGDlg::OnBnClickedCompress)
END_MESSAGE_MAP()


// CImageCompressionJPEGDlg message handlers

BOOL CImageCompressionJPEGDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	coeffCompress.SetRange(1, 3);
	coeffCompress.SetPos(3);
	compressType = coeffCompress.GetPos();
	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CImageCompressionJPEGDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CImageCompressionJPEGDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CImageCompressionJPEGDlg::OnBnClickedSearch()
{
	CFileDialog dlg(TRUE);
	int result = dlg.DoModal();
	if (result == IDOK)
	{
		image_path = dlg.GetPathName();
		UpdateData(FALSE);
	}

	Gdiplus::Bitmap bmp(image_path);
	image = bmp.Clone(0, 0, bmp.GetWidth(), bmp.GetHeight(), bmp.GetPixelFormat());
	width = bmp.GetWidth();
	height = bmp.GetHeight();

	if (width % 8 != 0 || height % 8 != 0)
	{
		int msgboxID = MessageBoxA(NULL, "Size of image should be aliquot to 8", "Error", MB_OK | MB_ICONWARNING);
		return;
	}

	picture.resize(height);
	for (int x = 0; x < height; ++x)
	{
		picture[x].resize(width);
		for (int y = 0; y < width; ++y)
		{
			Color color;
			bmp.GetPixel(x, y, &color);
			picture[x][y] = (color.GetR() + color.GetG() + color.GetB()) / 3;
		}
	}

	sizeImgRaw = width * height * 3 + headerSizeBmp;
	blockCount = width / blockSize * height / blockSize;
	
	// You can also set image directly to the window 
	//	gc1.SetBitmap(image);

	gc1.GradientCell(picture);
	gc1.MyPaint();

	UpdateData(FALSE);
}

void CImageCompressionJPEGDlg::Fourea(vector<complex<float>>& data, int is)
{
	int i, j, istep, n;
	n = data.size();
	int m, mmax;
	double r, r1, theta, w_r, w_i, temp_r, temp_i;
	double pi = 3.1415926f;

	r = pi * is;
	j = 0;
	for (i = 0; i < n; i++)
	{
		if (i < j)
		{
			temp_r = data[j].real();
			temp_i = data[j].imag();
			data[j] = data[i];
			data[i] = temp_r + complex <double>(0, 1) * temp_i;

		}
		m = n >> 1;
		while (j >= m) { j -= m; m = (m + 1) / 2; }
		j += m;
	}
	mmax = 1;
	while (mmax < n)
	{
		istep = mmax << 1;
		r1 = r / (double)mmax;
		for (m = 0; m < mmax; m++)
		{
			theta = r1 * m;
			w_r = (double)cos((double)theta);
			w_i = (double)sin((double)theta);
			for (i = m; i < n; i += istep)
			{
				j = i + mmax;
				temp_r = w_r * data[j].real() - w_i * data[j].imag();
				temp_i = w_r * data[j].imag() + w_i * data[j].real();
				data[j] = (data[i].real() - temp_r) + complex <double>(0, 1) * (data[i].imag() - temp_i);
				data[i] += (temp_r)+complex <double>(0, 1) * (temp_i);
			}
		}
		mmax = istep;
	}
	if (is > 0)
		for (i = 0; i < n; i++)
		{
			data[i] /= (double)n;
		}
}

void CImageCompressionJPEGDlg::Fourea_2D(vector<vector<complex<float>>>& data, int is)
{
	vector<vector<complex<float>>> tmp;
	for (int i = 0; i < data.size(); i++)
		Fourea(data[i], is);

	Transpose_2Dmatrix(data, tmp);

	for (int i = 0; i < tmp.size(); i++)
		Fourea(tmp[i], is);

	Transpose_2Dmatrix(tmp, data);
}

void CImageCompressionJPEGDlg::Transpose_2Dmatrix(vector<vector<complex<float>>> input, vector<vector<complex<float>>>& output)
{
	output.resize(input[0].size());
	for (int i = 0; i < input[0].size(); i++)
	{
		output[i].resize(input.size());
	}
	int m = input[0].size();
	int n = input.size();
	int k = 0;

	while (k < m) {
		for (int i = 0; i < n; i++) {
			output[k][i] = input[i][k];
		}
		k++;
	}
}

void CImageCompressionJPEGDlg::Transpose_2Dmatrix(vector<vector<float>> input, vector<vector<float>>& output)
{
	output.resize(input[0].size());
	for (int i = 0; i < input[0].size(); i++)
	{
		output[i].resize(input.size());
	}
	int m = input[0].size();
	int n = input.size();
	int k = 0;

	while (k < m) {
		for (int i = 0; i < n; i++) {
			output[k][i] = input[i][k];
		}
		k++;
	}
}

void CImageCompressionJPEGDlg::OnBnClickedCompress()
{
	UpdateData(TRUE);
	compressType = coeffCompress.GetPos();
	vector<vector<float>> block; //here 8x8 block
	CPoint ptrBlock = 0;
	sizeCompressImg = 0;

	resizeVector(spectrumOfBlock, blockSize);
	resizeVector(restoredPicture, picture.size());
	
	SetCursor(LoadCursor(nullptr, IDC_WAIT));
	for (int i = 1; i <= blockCount; i++) // going through the blocks count
	{
		comperessedCoef.clear();
		resizeVector(comperessedCoef, blockSize);
		GetBlockFromImg(picture, block, ptrBlock.x, ptrBlock.y); // getting one block 8x8 from image using two pointers
		//OutputVectorInFile(picture, "picture.txt");
		//OutputVectorInFile(block, "block.txt");
		for (int m = 0; m < block.size(); m++)
		{
			for (int n = 0; n < block.size(); n++)
			{
				complex<float> tmp;
				tmp = block[m][n];
				spectrumOfBlock[m][n] = tmp;
			}
		}
		// Doing FFT of that block
		Fourea_2D(spectrumOfBlock, -1);

		// TODO: need to save some information from that block on disc
		int mem = 0;
		switch (compressType)
		{
			case 3:
				// TODO: save the 6 coefficients with highest freqiency
				mem = Save12Coefficients();
				break;
			case 2:
				mem = Save24Coefficients();
				break;
			case 1:
				mem = Save48Coefficients();
				break;
		}
		sizeCompressImg += mem;

		// Backward FFT of compressedCoefficents block 8x8
		Fourea_2D(comperessedCoef, 1);

		for (int m = 0; m < blockSize; m++)
		{
			for (int n = 0; n < blockSize; n++)
				restoredPicture[m + ptrBlock.x][n + ptrBlock.y] = comperessedCoef[m][n].real();
		}

		// Switching to next block
		if (i % (width / blockSize) == 0)
		{
			ptrBlock.y += blockSize;
			ptrBlock.x = 0;
		}
		else ptrBlock.x += blockSize;

	}
	SetCursor(LoadCursor(nullptr, IDC_ARROW));

	gc2.GradientCell(restoredPicture);
	gc2.MyPaint();

	ratio = (double)sizeCompressImg * 100 / sizeImgRaw;
	deviation = CalcStandardDeviation(picture, restoredPicture);
	UpdateData(FALSE);
}

void CImageCompressionJPEGDlg::GetBlockFromImg(vector<vector<float>> input, vector<vector<float>> &block, int ptr_x, int ptr_y)
{	
	block.resize(blockSize);
	for (int i = 0; i < blockSize; i++)
		block[i].resize(blockSize);

	for (int i = 0; i < blockSize; i++)
	{
		for (int j = 0; j < blockSize; j++)
		{
			block[i][j] = input[ptr_x + i][ptr_y + j];
		}
	}
}

int CImageCompressionJPEGDlg::OutputVectorInFile(vector<vector<float>> v, string s)
{
	ofstream output_file(s);

	for (int i = 0; i < v.size(); i++)
	{
		for (int j = 0; j < v.size(); j++)
		{
			output_file << v[i][j] << "     "; 
		}

		output_file << endl;
	}
	return 0;
}

int CImageCompressionJPEGDlg::Save24Coefficients()
{
	Save12Coefficients();
	int last = spectrumOfBlock.size() - 1;
	//TODO: pick other coefficients for second compressing and create Save32Coefficients() function
	comperessedCoef[1][0].real(spectrumOfBlock[1][0].real());
	comperessedCoef[1][0].imag(spectrumOfBlock[1][0].imag());

	comperessedCoef[0][last - 1].real(spectrumOfBlock[0][last - 1].real());
	comperessedCoef[0][last - 1].imag(spectrumOfBlock[0][last - 1].imag());

	comperessedCoef[1][last].real(spectrumOfBlock[1][last].real());
	comperessedCoef[1][last].imag(spectrumOfBlock[1][last].imag());
	
	comperessedCoef[last - 1][last].real(spectrumOfBlock[last - 1][last].real());
	comperessedCoef[last - 1][last].imag(spectrumOfBlock[last - 1][last].imag());

	comperessedCoef[last][1].real(spectrumOfBlock[last][1].real());
	comperessedCoef[last][1].imag(spectrumOfBlock[last][1].imag());

	comperessedCoef[0][last - 1].real(spectrumOfBlock[0][last - 1].real());
	comperessedCoef[0][last - 1].imag(spectrumOfBlock[0][last - 1].imag());

	return GetSizeNonZeroValueInVector(comperessedCoef);
}

int CImageCompressionJPEGDlg::Save12Coefficients()
{
	int last = spectrumOfBlock.size() - 1;
	//left-up element
	comperessedCoef[0][0].real(spectrumOfBlock[0][0].real());
	comperessedCoef[0][0].imag(spectrumOfBlock[0][0].imag());
	//right-down element
	comperessedCoef[last][last].real(spectrumOfBlock[last][last].real());
	comperessedCoef[last][last].imag(spectrumOfBlock[last][last].imag());
	//left-down element
	comperessedCoef[last][0].real(spectrumOfBlock[last][0].real());
	comperessedCoef[last][0].imag(spectrumOfBlock[last][0].imag());
	//right-up element
	comperessedCoef[0][last].real(spectrumOfBlock[0][last].real());
	comperessedCoef[0][last].imag(spectrumOfBlock[0][last].imag());
	//plus 2 elements
	comperessedCoef[0][1].real(spectrumOfBlock[0][1].real());
	comperessedCoef[0][1].imag(spectrumOfBlock[0][1].imag());

	comperessedCoef[last][last - 1] = spectrumOfBlock[last][last - 1].real();
	comperessedCoef[last][last - 1] = spectrumOfBlock[last][last - 1].imag();

	return GetSizeNonZeroValueInVector(comperessedCoef);
}

int CImageCompressionJPEGDlg::Save48Coefficients()
{
	Save24Coefficients();
	int last = spectrumOfBlock.size() - 1;
	comperessedCoef[2][0].real(spectrumOfBlock[2][0].real());
	comperessedCoef[2][0].imag(spectrumOfBlock[2][0].imag());

	comperessedCoef[0][last - 2].real(spectrumOfBlock[0][last - 2].real());
	comperessedCoef[0][last - 2].imag(spectrumOfBlock[0][last - 2].imag());

	comperessedCoef[2][last].real(spectrumOfBlock[2][last].real());
	comperessedCoef[2][last].imag(spectrumOfBlock[2][last].imag());

	comperessedCoef[last - 2][last].real(spectrumOfBlock[last - 2][last].real());
	comperessedCoef[last - 2][last].imag(spectrumOfBlock[last - 2][last].imag());

	comperessedCoef[last][last - 2].real(spectrumOfBlock[last][last - 2].real());
	comperessedCoef[last][last - 2].imag(spectrumOfBlock[last][last - 2].imag());

	comperessedCoef[last][2].real(spectrumOfBlock[last][2].real());
	comperessedCoef[last][2].imag(spectrumOfBlock[last][2].imag());

	comperessedCoef[last - 2][0].real(spectrumOfBlock[last - 2][0].real());
	comperessedCoef[last - 2][0].imag(spectrumOfBlock[last - 2][0].imag());

	comperessedCoef[2][0].real(spectrumOfBlock[2][0].real());
	comperessedCoef[2][0].imag(spectrumOfBlock[2][0].imag());

	comperessedCoef[1][1].real(spectrumOfBlock[1][1].real());
	comperessedCoef[1][1].imag(spectrumOfBlock[1][1].imag());

	comperessedCoef[1][last - 1].real(spectrumOfBlock[1][last - 1].real());
	comperessedCoef[1][last - 1].imag(spectrumOfBlock[1][last - 1].imag());

	comperessedCoef[last - 1][last - 1].real(spectrumOfBlock[last - 1][last - 1].real());
	comperessedCoef[last - 1][last - 1].imag(spectrumOfBlock[last - 1][last - 1].imag());

	comperessedCoef[last - 1][1].real(spectrumOfBlock[last - 1][1].real());
	comperessedCoef[last - 1][1].imag(spectrumOfBlock[last - 1][1].imag());

	return GetSizeNonZeroValueInVector(comperessedCoef);
}

int CImageCompressionJPEGDlg::GetSizeNonZeroValueInVector(vector<vector<complex<float>>> v)
{
	int countNonZeroVal = 0;
	for (int i = 0; i < v.size(); i++)
	{
		for (int j = 0; j < v.size(); j++)
		{
			if (v[i][j].real() != 0) countNonZeroVal++;
			if(v[i][j].imag() != 0) countNonZeroVal++;
		}
	}

	return countNonZeroVal * sizeof(float);
}

double CImageCompressionJPEGDlg::CalcStandardDeviation(vector<vector<float>> v1, vector<vector<float>> v2)
{
	if(v1.size() != v2.size() || v1[0].size() != v2[0].size()) 
		int msgboxID = MessageBoxA(NULL, "Can't compute the standard deviation.\
											Reason: the dimensions of the vectors are not equal", "Error", MB_OK | MB_ICONWARNING);
	double diff = 0;
	for (int i = 0; i < v1.size(); i++)
	{
		for (int j = 0; j < v1.size(); j++)
		{
			diff += pow((v1[i][j] - v2[i][j]), 2);
		}
	}
	diff /= (v1.size() * v2.size());

	return diff;
}

