#pragma once

#include <afxwin.h>
#include <math.h>
#include <vector>
using namespace std;
using namespace Gdiplus;

#define M_PI 3.1415926535

class GDIClass :
	public CStatic
{
	DECLARE_DYNAMIC(GDIClass)
public:
	GDIClass();
	virtual ~GDIClass();
	virtual void DrawItem(LPDRAWITEMSTRUCT);
private:
protected:
	DECLARE_MESSAGE_MAP()
public:
	void MINMAX(vector<vector<float>> Data, double& Min, double& Max)
	{
		if (Data.size() == 0)return;
		Min = Max = 0;
		for (int i = 0; i < Data.size(); i++)
		{
			for (int j = 0; j < Data[i].size(); j++)
			{
				if (Data[i][j] < Min)Min = Data[i][j];
				if (Data[i][j] > Max)Max = Data[i][j];
			}
		}
	}

	int xmax = 100;
	int xmin = 0;
	int ymax = 100;
	int ymin = 0;
	Bitmap* im;

	struct GDIDot 
	{
		BYTE R=0;
		BYTE G=0;
		BYTE B=0;
	};
	std::vector <vector<GDIDot>> massPic;
	void MyPaint()
	{
		if (massPic.size() != 0)
		{
			xmax = massPic.size();
			ymax = massPic.size();
		}
		Invalidate();
	}
	void GradientCell(vector<vector<float>> Data)
	{
		massPic.clear();
		massPic.resize(Data.size());
		for (int i = 0; i < massPic.size(); i++)massPic[i].resize(Data[i].size());
		if (massPic.size() != 0)
		{
			double GradMin = 0;
			double GradMax = 0;
			MINMAX(Data, GradMin, GradMax);
			double GradRange = GradMax - GradMin;
			for (int i = 0; i < Data.size(); i++)
			{
				for (int j = 0; j < Data[i].size(); j++)
				{
					int buf = (int)(((Data[i][j] - GradMin) / GradRange) * 255);
					massPic[i][j].R = (BYTE)buf;
					massPic[i][j].G = (BYTE)buf;
					massPic[i][j].B = (BYTE)buf;
				}
			}
		}
	}
	void SetBitmap(Bitmap* bmp)
	{
		im = bmp;
		Invalidate(FALSE);
	}

};

