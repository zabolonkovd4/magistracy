﻿//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by TwoDimAntennaGrid.rc
//
#define IDD_TWODIMANTENNAGRID_DIALOG    102
#define IDR_MAINFRAME                   128
#define IDC_GRID                        1000
#define IDC_NEAR_POINT_X                1001
#define IDC_NEAR_POINT_Y                1002
#define IDC_RESET_ALL                   1003
#define IDC_SOURCE_COUNT                1005
#define IDC_SURFACE                     1006
#define IDC_RADIUS                      1007
#define IDC_BUTTON1                     1008
#define IDC_CALCULATE_I                 1008
#define IDC_BUTTON2                     1009
#define IDC_POINTER                     1009
#define IDC_STEP_PHI                    1010
#define IDC_EDIT2                       1011
#define IDC_STEP_THETTA                 1011

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1012
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
