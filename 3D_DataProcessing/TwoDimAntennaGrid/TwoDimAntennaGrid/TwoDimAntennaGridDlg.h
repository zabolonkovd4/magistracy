﻿// TwoDimAntennaGridDlg.h: файл заголовка
//

#pragma once
#include "chartdir.h"
#include <vector>
#include "ChartViewer.h"

using namespace std;

struct source_for_chart {
	double coord_plot;
	double coord;
};

struct source {
	double x;
	double y;
	double z = 0.;
};

// Диалоговое окно CTwoDimAntennaGridDlg
class CTwoDimAntennaGridDlg : public CDialogEx
{
// Создание
public:
	CTwoDimAntennaGridDlg(CWnd* pParent = nullptr);	// стандартный конструктор
	~CTwoDimAntennaGridDlg();

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_TWODIMANTENNAGRID_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// поддержка DDX/DDV


// Реализация
protected:
	HICON m_hIcon;

	// Созданные функции схемы сообщений
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnBtnClicked();
	afx_msg void OnClose();
	afx_msg void OnViewPortChanged();
	afx_msg void OnMouseMoveChart();
	afx_msg void OnMouseUpChart();

	DECLARE_MESSAGE_MAP()

private:
	CChartViewer cV;
public:
	struct spherePoint
	{
		double x;
		double y;
		double z;
		double intensity;
	};

	//variables
	double nearPntX;
	double nearPntY;
	int sourceCount;
	int xPlotStart = 55;
	int yPlotStart = 40;
	int widthPlot = 400;
	int heightPlot = 350;
	double scaleX = 50;
	double scaleY = 43.75;
	double radius;
	double lambda = 1;
	CChartViewer m_ChartViewer;
	double pointerStart_x, pointerStart_y;
	double pointerEnd_x, pointerEnd_y;
	CButton m_PointerPB;

	// 3D view angles
	double m_elevationAngle;
	double m_rotationAngle;

	// Keep track of mouse drag
	int m_lastMouseX;
	int m_lastMouseY;
	bool m_isDragging;

	//arrays and vectors
	vector<source_for_chart> sources_x;
	vector<source_for_chart> sources_y;
	vector<source> markPoint;
	vector<spherePoint> sphere;

	//functions
	CPoint getNearestPoint(double x, double y);
	void PlotSource(double xx = 0, double yy = 0);
	void PlotArea(XYChart* c);
	void PlotArea(vector<spherePoint> surface);
	void drawChart(CChartViewer* viewer);
	void buildSphere(double radius, double x0, double y0, double z0);
	double getIntensity(double sx, double sy, double sz);
	DoubleArray vectorToArray(std::vector<double>& v);
	afx_msg void OnBnClickedResetAll();
	afx_msg void OnBnClickedCalculateI();
	
	//pointers
	XYChart* xyc;	
	double step_phi;
	double step_thetta;
};
