﻿
// TwoDimAntennaGridDlg.cpp: файл реализации
//

#include "pch.h"
#include "framework.h"
#include "TwoDimAntennaGrid.h"
#include "TwoDimAntennaGridDlg.h"
#include "afxdialogex.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// Диалоговое окно CTwoDimAntennaGridDlg
#define PI 3.14159265358979323846   // pi


CTwoDimAntennaGridDlg::CTwoDimAntennaGridDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_TWODIMANTENNAGRID_DIALOG, pParent)
	, nearPntX(0)
	, nearPntY(0)
	, sourceCount(49)
	, radius(200)
	, step_phi(0.05)
	, step_thetta(0.05)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

CTwoDimAntennaGridDlg::~CTwoDimAntennaGridDlg()
{
	delete m_ChartViewer.getChart();
}

void CTwoDimAntennaGridDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_GRID, cV);
	DDX_Text(pDX, IDC_NEAR_POINT_X, nearPntX);
	DDX_Text(pDX, IDC_NEAR_POINT_Y, nearPntY);
	DDX_Text(pDX, IDC_SOURCE_COUNT, sourceCount);
	DDX_Text(pDX, IDC_RADIUS, radius);
	DDV_MinMaxDouble(pDX, radius, 0, DBL_MAX);
	DDX_Control(pDX, IDC_SURFACE, m_ChartViewer);
	DDX_Control(pDX, IDC_POINTER, m_PointerPB);
	DDX_Text(pDX, IDC_STEP_PHI, step_phi);
	DDX_Text(pDX, IDC_STEP_THETTA, step_thetta);
}

BEGIN_MESSAGE_MAP(CTwoDimAntennaGridDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_CONTROL(BN_CLICKED, IDC_GRID, OnBtnClicked)
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_RESET_ALL, &CTwoDimAntennaGridDlg::OnBnClickedResetAll)
	ON_BN_CLICKED(IDC_CALCULATE_I, &CTwoDimAntennaGridDlg::OnBnClickedCalculateI)
	ON_CONTROL(CVN_ViewPortChanged, IDC_SURFACE, OnViewPortChanged)
	ON_CONTROL(CVN_MouseMoveChart, IDC_SURFACE, OnMouseMoveChart)
	ON_CONTROL(BN_CLICKED, IDC_SURFACE, OnMouseUpChart)
END_MESSAGE_MAP()


// Обработчики сообщений CTwoDimAntennaGridDlg

BOOL CTwoDimAntennaGridDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Задает значок для этого диалогового окна.  Среда делает это автоматически,
	//  если главное окно приложения не является диалоговым
	SetIcon(m_hIcon, TRUE);			// Крупный значок
	SetIcon(m_hIcon, FALSE);		// Мелкий значок
	m_ChartViewer.setMouseUsage(-1);

	PlotSource();

	// Update the viewport to display the chart
	m_ChartViewer.updateViewPort(true, false);

	return TRUE;  // возврат значения TRUE, если фокус не передан элементу управления
}

// При добавлении кнопки свертывания в диалоговое окно нужно воспользоваться приведенным ниже кодом,
//  чтобы нарисовать значок.  Для приложений MFC, использующих модель документов или представлений,
//  это автоматически выполняется рабочей областью.

void CTwoDimAntennaGridDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // контекст устройства для рисования

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Выравнивание значка по центру клиентского прямоугольника
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Нарисуйте значок
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// Система вызывает эту функцию для получения отображения курсора при перемещении
//  свернутого окна.
HCURSOR CTwoDimAntennaGridDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CTwoDimAntennaGridDlg::OnBtnClicked()
{
	UpdateData(TRUE);
	int x = cV.getChartMouseX();
	int y = cV.getChartMouseY();
	CPoint newPoint = getNearestPoint(x, y);
	PlotSource(newPoint.x, newPoint.y);
}

CPoint CTwoDimAntennaGridDlg::getNearestPoint(double x, double y)
{
	CPoint result;
	int count = 0;
	for (int i = 0; i < sources_x.size(); i++)
	{
		if (fabs(sources_x[i].coord_plot - x) < scaleX / 2) {
			result.x = sources_x[i].coord;
			count++;
		}
		if (fabs(sources_y[i].coord_plot - y) < scaleY / 2)
		{
			result.y = sources_y[i].coord;
			count++;
		}
		else continue;
	}
	nearPntX = result.x;
	nearPntY = result.y;
	UpdateData(FALSE);
	return result;
}

void CTwoDimAntennaGridDlg::PlotSource(double xx, double yy)
{
	XYChart* c = new XYChart(widthPlot + 100, heightPlot + 100);

	PlotArea(c);
	// Add each symbol as a separate scatter layer.
	if (xx != 0. || yy != 0.)
	{
		source buf;
		buf.x = xx;
		buf.y = yy;
		markPoint.push_back(buf);

		for (int i = 0; i < markPoint.size(); i++)
		{
			double coor1[] = { markPoint[i].x };
			double coor2[] = { markPoint[i].y };
			c->addScatterLayer(DoubleArray(coor1, (int)(sizeof(coor1) / sizeof(coor1[0]))), DoubleArray(
				coor2, (int)(sizeof(coor2) / sizeof(coor2[0]))), "", Chart::StarShape(4), 35, 0xfff000, 1);
		}
		
	}
	cV.setChart(c);
}

void CTwoDimAntennaGridDlg::PlotArea(XYChart *c)
{
	UpdateData(TRUE);
	// Set the plotarea at (xPlotStart, yPlotStart) and of size widthPlot x heightPlot pixels, with a light grey border
	// (0xc0c0c0). Turn on both horizontal and vertical grid lines with light grey color (0xc0c0c0)
	c->setPlotArea(xPlotStart, yPlotStart, widthPlot, heightPlot, -1, -1, 0xc0c0c0, 0xc0c0c0, -1);

	// Set the axes line width to 3 pixels
	c->xAxis()->setWidth(3);
	c->yAxis()->setWidth(3);

	// Ensure the ticks are at least 1 unit part (integer ticks)
	c->xAxis()->setMinTickInc(1);
	c->yAxis()->setMinTickInc(1);

	sources_x.clear();
	sources_x.resize(sourceCount);
	sources_y.clear();
	sources_y.resize(sourceCount);
	scaleX = (widthPlot - xPlotStart) / (int)sqrt(sourceCount);
	scaleY = (heightPlot - yPlotStart) / (int)sqrt(sourceCount);

	
	for (int i = 0; i < sourceCount; i++)
	{
		double buf = i % (int)sqrt(sourceCount) + 1.;
		double x = buf;
		double y = buf;
		double coor1[] = { buf };
		double coor2[] = { i  / (int)sqrt(sourceCount) + 1.};
		sources_x[i].coord = x;
		sources_y[i].coord = y;
		sources_x[i].coord_plot = x * scaleX + xPlotStart;
		sources_y[i].coord_plot = heightPlot - y * scaleY + yPlotStart;
		c->addScatterLayer(DoubleArray(coor1, (int)(sizeof(coor1) / sizeof(coor1[0]))), DoubleArray(
			coor2, (int)(sizeof(coor2) / sizeof(coor2[0]))), "", Chart::StarShape(4), 15, 0xffffff, 1);
	}
}
DoubleArray CTwoDimAntennaGridDlg::vectorToArray(std::vector<double>& v)
{
	return (v.size() == 0) ? DoubleArray() : DoubleArray(v.data(), (int)v.size());
}

void CTwoDimAntennaGridDlg::PlotArea(vector<spherePoint> surface)
{
	SurfaceChart* sc = new SurfaceChart(600, 560);
	sc->setViewAngle(m_elevationAngle, m_rotationAngle);
	sc->addTitle("Surface Intensity Allocation   ", "timesi.ttf", 17);
	sc->setPlotRegion(300, 270, 360, 360, 270);

	std::vector<double> Ve_x;
	std::vector<double> Ve_y;
	std::vector<double> Ve_z;

	for (int i = 0; i < surface.size(); i++)
	{
		int aaa = sqrt((double)sourceCount);
		double centr = sqrt((double)sourceCount)/2;
		double d = sqrt(pow(surface[i].x - centr, 2) + pow(surface[i].y - centr, 2));
		Ve_x.push_back(surface[i].x);
		Ve_y.push_back(surface[i].y);
		Ve_z.push_back(surface[i].intensity);	
	}

	
		sc->setData(vectorToArray(Ve_x), vectorToArray(Ve_y), vectorToArray(Ve_z));
		sc->setColorAxis(645, 270, Chart::Left, 200, Chart::Right);

		sc->xAxis()->setTitle("X (m)", "arialbd.ttf", 10);
		sc->yAxis()->setTitle("Y (m)", "arialbd.ttf", 10);
		sc->xAxis()->setLinearScale(-radius, radius);
		sc->yAxis()->setLinearScale(-radius, radius);
		sc->zAxis()->setTitle("Intensity Allocation (J/m<*font,super*>2<*/font*>)", "arialbd.ttf", 10);
		sc->setViewAngle(m_elevationAngle, m_rotationAngle);
	m_ChartViewer.setChart(sc);
	delete sc;
}

void CTwoDimAntennaGridDlg::buildSphere(double radius, double x0, double y0, double z0)
{
	sphere.clear();
	int number = 0;
	for (double phi = 0.; phi < 2 * PI; phi += step_phi)
	{
		for (double thetta = 0; thetta < PI / 2; thetta += step_thetta)
		{
			spherePoint tmp;
			tmp.x = x0 + radius * sin(thetta) * cos(phi);
			tmp.y = y0 + radius * sin(thetta) * sin(phi);
			tmp.z = z0 + radius * cos(thetta);
			tmp.intensity = getIntensity(tmp.x, tmp.y, tmp.z);
			sphere.push_back(tmp);
		}
		number++;
	}

}

double CTwoDimAntennaGridDlg::getIntensity(double sx, double sy, double sz)
{
	double intensity = 0;
	double a0 = 1.;
	double k = 2 * PI / lambda;
	double re = 0;
	double im = 0;
	for (int i = 0; i < markPoint.size(); i++)
	{
		double r = sqrt(pow(markPoint[i].x - sx, 2) + pow(markPoint[i].y - sy, 2) + pow(markPoint[i].z - sz, 2));
		re += cos(2 * PI * r / lambda) / r;
		im += sin(2 * PI * r / lambda) / r;
	}
	intensity = sqrt(re * re + im * im);
	return intensity;
}


void CTwoDimAntennaGridDlg::OnClose()
{
	delete xyc;
	CDialogEx::OnClose();
}


void CTwoDimAntennaGridDlg::OnBnClickedResetAll()
{
	markPoint.clear();
	XYChart* c = new XYChart(widthPlot + 100, heightPlot + 100);
	PlotArea(c);
	cV.setChart(c);
	delete c;
}




void CTwoDimAntennaGridDlg::OnBnClickedCalculateI()
{
	UpdateData(TRUE);
	double centr = sqrt((double)sourceCount) / 2;
	buildSphere(radius, centr, centr, 0);
	
	drawChart(&m_ChartViewer);
}

void CTwoDimAntennaGridDlg::OnViewPortChanged()
{
	if (m_ChartViewer.needUpdateChart())
		drawChart(&m_ChartViewer);
}


void CTwoDimAntennaGridDlg::drawChart(CChartViewer* viewer)
{
	SurfaceChart* sc = new SurfaceChart(600, 560);
	sc->setViewAngle(m_elevationAngle, m_rotationAngle);
	sc->addTitle("Surface Intensity Allocation   ", "timesi.ttf", 17);
	sc->setPlotRegion(300, 270, 360, 360, 270);

	std::vector<double> Ve_x;
	std::vector<double> Ve_y;
	std::vector<double> Ve_z;

	for (int i = 0; i < sphere.size(); i++)
	{
		int aaa = sqrt((double)sourceCount);
		double centr = sqrt((double)sourceCount) / 2;
		double d = sqrt(pow(sphere[i].x - centr, 2) + pow(sphere[i].y - centr, 2));

		Ve_x.push_back(sphere[i].x);
		Ve_y.push_back(sphere[i].y);
		Ve_z.push_back(sphere[i].intensity);
	}


	sc->setData(vectorToArray(Ve_x), vectorToArray(Ve_y), vectorToArray(Ve_z));
	sc->setColorAxis(645, 270, Chart::Left, 200, Chart::Right);

	sc->xAxis()->setTitle("X (m)", "arialbd.ttf", 10);
	sc->yAxis()->setTitle("Y (m)", "arialbd.ttf", 10);
	sc->xAxis()->setLinearScale(-radius, radius);
	sc->yAxis()->setLinearScale(-radius, radius);
	sc->zAxis()->setTitle("Intensity Allocation (J/m<*font,super*>2<*/font*>)", "arialbd.ttf", 10);
	
	// Spline interpolate data to a 80 x 80 grid for a smooth surface
	sc->setInterpolation(80, 80);

	// Set the view angles
	sc->setViewAngle(m_elevationAngle, m_rotationAngle);

	// Check if draw frame only during rotation
	if (m_isDragging)
		sc->setShadingMode(Chart::RectangularFrame);


	// Output the chart
	delete viewer->getChart();
	viewer->setChart(sc);
}

void CTwoDimAntennaGridDlg::OnMouseMoveChart()
{
	int mouseX = m_ChartViewer.getChartMouseX();
	int mouseY = m_ChartViewer.getChartMouseY();

	// Drag occurs if mouse button is down and the mouse is captured by the m_ChartViewer
	if (((GetKeyState(VK_LBUTTON) & 0x100) != 0) && (GetCapture() == &m_ChartViewer))
	{
		if (m_isDragging)
		{
			// The chart is configured to rotate by 90 degrees when the mouse moves from 
			// left to right, which is the plot region width (360 pixels). Similarly, the
			// elevation changes by 90 degrees when the mouse moves from top to buttom,
			// which is the plot region height (270 pixels).
			m_rotationAngle += (m_lastMouseX - mouseX) * 90.0 / 360;
			m_elevationAngle += (mouseY - m_lastMouseY) * 90.0 / 270;
			m_ChartViewer.updateViewPort(true, false);
		}

		// Keep track of the last mouse position
		m_lastMouseX = mouseX;
		m_lastMouseY = mouseY;
		m_isDragging = true;
	}
}

void CTwoDimAntennaGridDlg::OnMouseUpChart()
{
	m_isDragging = false;
	m_ChartViewer.updateViewPort(true, false);
}