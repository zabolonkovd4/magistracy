﻿
// TwoDimAntennaGrid.h: главный файл заголовка для приложения PROJECT_NAME
//

#pragma once

#ifndef __AFXWIN_H__
	#error "включить pch.h до включения этого файла в PCH"
#endif

#include "resource.h"		// основные символы


// CTwoDimAntennaGridApp:
// Сведения о реализации этого класса: TwoDimAntennaGrid.cpp
//

class CTwoDimAntennaGridApp : public CWinApp
{
public:
	CTwoDimAntennaGridApp();

// Переопределение
public:
	virtual BOOL InitInstance();

// Реализация

	DECLARE_MESSAGE_MAP()
};

extern CTwoDimAntennaGridApp theApp;
