//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by NoiseCleaner.rc
//
#define IDD_NOISECLEANER_DIALOG         102
#define IDR_MAINFRAME                   128
#define IDC_A1                          1000
#define IDC_A2                          1001
#define IDC_A3                          1002
#define IDC_A4                          1003
#define IDC_A5                          1004
#define IDC_SIGMA_X1                    1005
#define IDC_SIGMA_X2                    1006
#define IDC_SIGMA_X3                    1007
#define IDC_SIGMA_X4                    1008
#define IDC_SIGMA_X5                    1009
#define IDC_SIGMA_Y1                    1010
#define IDC_SIGMA_Y2                    1011
#define IDC_SIGMA_Y3                    1012
#define IDC_SIGMA_Y4                    1013
#define IDC_SIGMA_Y5                    1014
#define IDC_X1                          1015
#define IDC_X2                          1016
#define IDC_X3                          1017
#define IDC_X4                          1018
#define IDC_X5                          1019
#define IDC_Y1                          1020
#define IDC_Y2                          1021
#define IDC_Y3                          1022
#define IDC_Y4                          1023
#define IDC_Y5                          1024
#define IDC_BUILD_BTN                   1025
#define IDC_GAUSS_SIZE                  1026
#define IDC_INPUT_PIC                   1027
#define IDC_ENERGY_PERCENT              1029
#define IDC_OUTPUT_PIC                  1030
#define IDC_INPUT_SPECTRUM              1031
#define IDC_BUTTON1                     1033
#define IDC_FOURIE                      1033
#define IDC_ENERGY_THRESHOLD            1034
#define IDC_CLEAR_SP                    1035
#define IDC_BUTTON2                     1036
#define IDC_RECOVER_BTN                 1036
#define IDC_BUTTON3                     1037
#define IDC_IMAGE                       1037
#define IDC_DIFF1                       1038
#define IDC_DIFF2                       1039
#define IDC_CHECK_LOG_SCALE             1040
#define IDC_IMG_NAME                    1041
#define IDC_BUTTON4                     1042
#define IDC_SEARCH                      1042

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1043
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
