﻿// NoiseCleanerDlg.cpp : implementation file
//
#include "pch.h"
#include "framework.h"
#include "NoiseCleaner.h"
#include "NoiseCleanerDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CNoiseCleanerDlg dialog

CNoiseCleanerDlg::CNoiseCleanerDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_NOISECLEANER_DIALOG, pParent)
	, ampl1(50)
	, ampl2(50)
	, ampl3(50)
	, ampl4(50)
	, ampl5(50)
	, sigma1_x(50)
	, sigma2_x(50)
	, sigma3_x(50)
	, sigma4_x(50)
	, sigma5_x(50)
	, sigma1_y(50)
	, sigma2_y(50)
	, sigma3_y(50)
	, sigma4_y(50)
	, sigma5_y(50)
	, x1(0.2 * 500)
	, x2(0.2 * 500)
	, x3(0.8 * 500)
	, x4(0.8 * 500)
	, x5(0.5 * 500)
	, y1(0.2 * 500)
	, y2(0.8 * 500)
	, y3(0.8 * 500)
	, y4(0.2 * 500)
	, y5(0.5 * 500)
	, N(500)
	, d(0)
	, energyThreshold(15)
	, diff_input_recovered(0)
	, diff_noised_input(0)
	, img_path(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

}

void CNoiseCleanerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_INPUT_PIC, gc1);
	DDX_Control(pDX, IDC_INPUT_SPECTRUM, gc2);
	DDX_Control(pDX, IDC_OUTPUT_PIC, gc3);
	DDX_Text(pDX, IDC_A1, ampl1);
	DDX_Text(pDX, IDC_A2, ampl2);
	DDX_Text(pDX, IDC_A3, ampl3);
	DDX_Text(pDX, IDC_A4, ampl4);
	DDX_Text(pDX, IDC_A5, ampl5);
	DDX_Text(pDX, IDC_SIGMA_X1, sigma1_x);
	DDX_Text(pDX, IDC_SIGMA_X2, sigma2_x);
	DDX_Text(pDX, IDC_SIGMA_X3, sigma3_x);
	DDX_Text(pDX, IDC_SIGMA_X4, sigma4_x);
	DDX_Text(pDX, IDC_SIGMA_X5, sigma5_x);
	DDX_Text(pDX, IDC_SIGMA_Y1, sigma1_y);
	DDX_Text(pDX, IDC_SIGMA_Y2, sigma2_y);
	DDX_Text(pDX, IDC_SIGMA_Y3, sigma3_y);
	DDX_Text(pDX, IDC_SIGMA_Y4, sigma4_y);
	DDX_Text(pDX, IDC_SIGMA_Y5, sigma5_y);
	DDX_Text(pDX, IDC_X1, x1);
	DDX_Text(pDX, IDC_X2, x2);
	DDX_Text(pDX, IDC_X3, x3);
	DDX_Text(pDX, IDC_X4, x4);
	DDX_Text(pDX, IDC_X5, x5);
	DDX_Text(pDX, IDC_Y1, y1);
	DDX_Text(pDX, IDC_Y2, y2);
	DDX_Text(pDX, IDC_Y3, y3);
	DDX_Text(pDX, IDC_Y4, y4);
	DDX_Text(pDX, IDC_Y5, y5);
	DDX_Text(pDX, IDC_GAUSS_SIZE, N);
	DDX_Text(pDX, IDC_ENERGY_PERCENT, d);
	DDX_Text(pDX, IDC_ENERGY_THRESHOLD, energyThreshold);
	DDV_MinMaxInt(pDX, energyThreshold, 0, 100);
	DDX_Text(pDX, IDC_DIFF1, diff_input_recovered);
	DDX_Text(pDX, IDC_DIFF2, diff_noised_input);
	DDX_Control(pDX, IDC_CHECK_LOG_SCALE, isLogScale);
	DDX_Text(pDX, IDC_IMG_NAME, img_path);
}

BEGIN_MESSAGE_MAP(CNoiseCleanerDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUILD_BTN, &CNoiseCleanerDlg::OnBnClickedBuildBtn)
	ON_BN_CLICKED(IDC_FOURIE, &CNoiseCleanerDlg::OnBnClickedFourie)
	ON_BN_CLICKED(IDC_CLEAR_SP, &CNoiseCleanerDlg::OnBnClickedClearSp)
	ON_BN_CLICKED(IDC_RECOVER_BTN, &CNoiseCleanerDlg::OnBnClickedRecoverBtn)
	ON_BN_CLICKED(IDC_IMAGE, &CNoiseCleanerDlg::OnBnClickedImage)
	ON_BN_CLICKED(IDC_SEARCH, &CNoiseCleanerDlg::OnBnClickedSearch)
END_MESSAGE_MAP()


// CNoiseCleanerDlg message handlers

BOOL CNoiseCleanerDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	isBMP = false;
	isLogScale.SetCheck(1);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CNoiseCleanerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CNoiseCleanerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

double CNoiseCleanerDlg::GetGauss3D(double ampl, int i, int j, double x0, double y0, double sigma_x, double sigma_y)
{
	double result = ampl * exp(-(((i - x0) * (i - x0)) / pow(sigma1_x, 2) + ((j - y0) * (j - y0)) / pow(sigma_y, 2)));
	return result;
}

void CNoiseCleanerDlg::OnBnClickedBuildBtn()
{
	UpdateData(TRUE);
	clearAllVector();
	isBMP = false;
	// Initialization 5 gauss domes
	input_gauss.clear();
	interpolated_gauss.clear();
	input_gauss.resize(N);
	for (int i = 0; i < N; i++)
	{
		input_gauss[i].resize(N);
		for (int j = 0; j < N; j++)
		{
			input_gauss[i][j] += GetGauss3D(ampl1, i, j, x1, y1, sigma1_x, sigma1_y) +
				GetGauss3D(ampl2, i, j, x2, y2, sigma2_x, sigma2_y) +
				GetGauss3D(ampl3, i, j, x3, y3, sigma3_x, sigma3_y) +
				GetGauss3D(ampl4, i, j, x4, y4, sigma4_x, sigma4_y) +
				GetGauss3D(ampl5, i, j, x5, y5, sigma5_x, sigma5_y);
		}
	}
	
	N = Bilinear_interpolation(input_gauss, interpolated_gauss);

	AddNoise(d, interpolated_gauss, interpolated_noised_gauss);
	diff_noised_input = CalculateDifference(interpolated_noised_gauss, interpolated_gauss);

	gc1.im = nullptr;
	//Plot gradient image
	if (d == 0) gc1.GradientCell(interpolated_gauss);
	else gc1.GradientCell(interpolated_noised_gauss);
	gc1.MyPaint();
	UpdateData(FALSE);
}

void CNoiseCleanerDlg::Transpose_2Dmatrix(vector<vector<double>> input, vector<vector<double>>& output)
{
	output.resize(input[0].size());
	for (int i = 0; i < input[0].size(); i++)
	{
		output[i].resize(input.size());
	}
	int m = input[0].size();
	int n = input.size();
	int k = 0;

	while (k < m) {
		for (int i = 0; i < n; i++) {
			output[k][i] = input[i][k];
		}
		k++;
	}
}

void CNoiseCleanerDlg::GetModuleSpectrum(vector<vector<complex<double>>> input, vector<vector<double>>& output)
{
	int size = input.size();
	output.resize(size);
	for (int i = 0; i < size; i++)
	{
		output[i].resize(size);
		for (int j = 0; j < size; j++)
		{
			double mod = sqrt(input[i][j].real() * input[i][j].real() + input[i][j].imag() * input[i][j].imag());
			if (isLogScale.GetCheck())
				output[i][j] = log(1. + mod);
			else output[i][j] = mod;
		}
	}

	output[0][0] = 0;
}

void CNoiseCleanerDlg::SwitchSqOfSpectrum(vector<vector<double>>& sp)
{
	int halfSize = sp.size() / 2;
	int fullSize = sp.size();
	//Switching 1th with 4th square
	for (int i = halfSize; i < fullSize; i++)
	{
		for (int j = 0; j < halfSize; j++)
		{
			double oneToFour;
			oneToFour = sp[i][j];
			sp[i][j] = sp[i - halfSize][j + halfSize];
			sp[i - halfSize][j + halfSize] = oneToFour;
		}
	}
	//Switching 2nd with 3rd square
	for (int i = halfSize; i < fullSize; i++)
	{
		for (int j = halfSize; j < fullSize; j++)
		{
			double twoToThree;
			twoToThree = sp[i][j];
			sp[i][j] = sp[i - halfSize][j - halfSize];
			sp[i - halfSize][j - halfSize] = twoToThree;
		}
	}

}

void CNoiseCleanerDlg::ClearSpectrum(vector<vector<complex<double>>> &sp, int threshold, vector<vector<double>> &output)
{
	output.resize(sp.size());
	for (int i = 0; i < N; i++)
		output[i].resize(sp.size());

	double energy = 0;
	double fullEnergy = GetEnergy(sp);
	
	int s = sp.size();
	int range = 0;
	double tmp = 0;
	while (energy < threshold * fullEnergy / 100)
	{
		energy = 0;
		int i, j;
		//leftDown square
		for (i = 0; i < range; i++)
		{
			for (j = 0; j < range; j++)
			{
				energy += sqrt(pow(sp[i][j].real() * sp[i][j].imag(), 2) + pow(sp[i][j].real() * sp[i][j].imag(), 2));
			}
		}
		//leftUp square
		for (i = s - range; i < s; i++)
		{
			for (j = 0; j < range; j++)
			{
				energy += sqrt(pow(sp[i][j].real() * sp[i][j].imag(), 2) + pow(sp[i][j].real() * sp[i][j].imag(), 2));
			}
		}
		energy += tmp;
		tmp = 0;

		//rightUp
		for (i = s - range; i < s; i++)
		{
			for (j = s - range; j < s; j++)
			{
				energy += sqrt(pow(sp[i][j].real() * sp[i][j].imag(), 2) + pow(sp[i][j].real() * sp[i][j].imag(), 2));
			}
		}
		energy += tmp;
		tmp = 0;

		//rightDown
		for (i = 0; i < range; i++)
		{
			for (j = s - range; j < s; j++)
			{
				energy += sqrt(pow(sp[i][j].real() * sp[i][j].imag(), 2) + pow(sp[i][j].real() * sp[i][j].imag(), 2));
			}
		}
		range++;

	}
	//clearing spectrum
	for (int i = range; i < sp.size() - range; i++)
	{
		for (int j = 0; j < sp.size(); j++)
		{
			sp[i][j] = 0;
		}
	}

	for (int i = 0; i < sp.size(); i++)
	{
		for (int j = range; j < sp.size() - range; j++)
		{
			sp[i][j] = 0;
		}
	}
	GetModuleSpectrum(sp, output);
	SwitchSqOfSpectrum(output);
}

double CNoiseCleanerDlg::GetEnergy(vector<vector<complex<double>>> v)
{
	int s = v.size();
	double fullEnergy = 0;
	for (int i = 0; i < s; i++)
	{
		for (int j = 0; j < s; j++)
		{
			fullEnergy += sqrt(pow(v[i][j].real() * v[i][j].imag(), 2) + pow(v[i][j].real() * v[i][j].imag(), 2));
		}
	}
	return fullEnergy;
}

void CNoiseCleanerDlg::AddNoise(int d, vector<vector<double>> input, vector<vector<double>> &output)
{
	if (d != 0) {
		//add noise
		double buf = 0;
		double alf = 0;
		double En = 0;
		double Es = 0;
		int p = 20;
		vector<double> m;
		vector<vector<double>> S;
		m.clear();
		S.clear();
		S.resize(N);

		for (int i = 0; i < N; i++)
		{
			S[i].resize(N);
			for (int j = 0; j < N; j++)
			{
				vector<double> s;
				s.resize(p);
				for (int k = 0; k < p; k++)
				{
					s[k] = -1 + 2 * rand() * 1. / RAND_MAX;
					S[i][j] = (S[i][j] + s[k]);
				}
				S[i][j] = S[i][j] / p;
				En += pow(S[i][j], 2);
				Es += pow(input[i][j], 2);
			}
		}

		buf = d * Es / 100;
		alf = sqrt(buf / En);

		output.resize(N);
		for (int i = 0; i < N; i++)
		{
			output[i].resize(N);
			for (int j = 0; j < N; j++)
			{
				input[i][j] = input[i][j] + alf * S[i][j];
			}
		}
		output = input;
	}
	else output = input;
}

void CNoiseCleanerDlg::clearAllVector()
{
	input_gauss.clear();
	interpolated_gauss.clear();
	spectrum.clear();
	recovered_gauss.clear();
	picture.clear();
	interpolated_picture.clear();
}

double CNoiseCleanerDlg::CalculateDifference(vector<vector<double>> signal1, vector<vector<double>> signal2)
{
	double diff = 0;
	double sum = 0;
	for (int i = 0; i < signal1.size(); i++)
	{
		for (int j = 0; j < signal1.size(); j++)
		{
			diff += pow((signal1[i][j] - signal2[i][j]), 2);
			sum += pow(signal2[i][j], 2);
		}
	}
	diff /= sum;
	return diff;
}

double CNoiseCleanerDlg::GetEnergy(vector<vector<double>> v)
{
	int s = v.size();
	double fullEnergy = 0;
	for (int i = 0; i < s; i++)
	{
		for (int j = 0; j < s; j++)
		{
			fullEnergy += sqrt(pow(v[i][j], 2) + pow(v[i][j], 2));
		}
	}
	return fullEnergy;
}
void CNoiseCleanerDlg::Transpose_2Dmatrix(vector<vector<complex<double>>> input, vector<vector<complex<double>>>& output)
{
	output.resize(input[0].size());
	for (int i = 0; i < input[0].size(); i++)
	{
		output[i].resize(input.size());
	}
	int m = input[0].size();
	int n = input.size();
	int k = 0;

	while (k < m) {
		for (int i = 0; i < n; i++) {
			output[k][i] = input[i][k];
		}
		k++;
	}
}

void CNoiseCleanerDlg::Fourea(vector <complex<double>>& data, int is)
{
	int i, j, istep, n;
	n = data.size();
	int m, mmax;
	double r, r1, theta, w_r, w_i, temp_r, temp_i;
	double pi = 3.1415926f;

	r = pi * is;
	j = 0;
	for (i = 0; i < n; i++)
	{
		if (i < j)
		{
			temp_r = data[j].real();
			temp_i = data[j].imag();
			data[j] = data[i];
			data[i] = temp_r + complex <double>(0, 1) * temp_i;

		}
		m = n >> 1;
		while (j >= m) { j -= m; m = (m + 1) / 2; }
		j += m;
	}
	mmax = 1;
	while (mmax < n)
	{
		istep = mmax << 1;
		r1 = r / (double)mmax;
		for (m = 0; m < mmax; m++)
		{
			theta = r1 * m;
			w_r = (double)cos((double)theta);
			w_i = (double)sin((double)theta);
			for (i = m; i < n; i += istep)
			{
				j = i + mmax;
				temp_r = w_r * data[j].real() - w_i * data[j].imag();
				temp_i = w_r * data[j].imag() + w_i * data[j].real();
				data[j] = (data[i].real() - temp_r) + complex <double>(0, 1) * (data[i].imag() - temp_i);
				data[i] += (temp_r)+complex <double>(0, 1) * (temp_i);
			}
		}
		mmax = istep;
	}
	if (is > 0)
		for (i = 0; i < n; i++)
		{
			data[i] /= (double)n;
		}
}

void CNoiseCleanerDlg::Fourea_2D(vector<vector<complex<double>>>& data, int is)
{
	vector<vector<complex<double>>> tmp;
	for (int i = 0; i < data.size(); i++)
		Fourea(data[i], is);

	Transpose_2Dmatrix(data, tmp);

	for (int i = 0; i < tmp.size(); i++)
		Fourea(tmp[i], is);

	Transpose_2Dmatrix(tmp, data);

}

void CNoiseCleanerDlg::Linear_interpolation(vector<double> input, vector<double>& output)
{
	output.clear();
	int oldLenght = input.size();
	int newLenght = Step2(oldLenght);
	int rightNeighbour = 0;
	output.push_back(input[0]);

	double newStep = (double)(input.size() - 1) / ((double)newLenght - 1);
	for (int j = 1; j < newLenght; j++)
	{
		double newPosition = newStep * j;
		rightNeighbour = (int)ceil(newPosition);
		output.push_back(((double)rightNeighbour - newPosition) * input[rightNeighbour - 1] + (newPosition - rightNeighbour + 1) * input[rightNeighbour]);
	}
}


//check if it works!!!
int CNoiseCleanerDlg::Bilinear_interpolation(vector<vector<double>> input, vector<vector<double>>& output)
{
	output.clear();
	//horizontal interpolation
	for (int i = 0; i < input.size(); i++)
	{
		vector<double> buffer;
		Linear_interpolation(input[i], buffer);
		output.push_back(buffer);
	}

	vector<vector<double>> onceTransposedData;
	Transpose_2Dmatrix(output, onceTransposedData);

	output.clear();

	vector<vector<double>> twiceTransposedData;

	//vertical interpolation
	for (int i = 0; i < onceTransposedData.size(); i++)
	{
		vector<double> buffer;
		Linear_interpolation(onceTransposedData[i], buffer);
		twiceTransposedData.push_back(buffer);
	}

	Transpose_2Dmatrix(twiceTransposedData, output);

	return output.size();
}

int CNoiseCleanerDlg::Step2(int sizein)
{
	int i = 0;
	double S = sizein;
	for (;;)
	{
		if (S > 1)
		{
			i++;
			S /= 2;
		}
		else break;
	}
	return pow(2, i);
}


void CNoiseCleanerDlg::OnBnClickedFourie()
{
	UpdateData(TRUE);
	if (!isBMP) {
		spectrum.resize(N);
		for (int i = 0; i < N; i++)
		{
			spectrum[i].resize(N);
			for (int j = 0; j < N; j++)
			{
				complex<double> tmp;
				tmp = interpolated_noised_gauss[i][j];
				spectrum[i][j] = tmp;
			}
		}

		Fourea_2D(spectrum, -1);

		vector<vector<double>> spectrum_module;

		GetModuleSpectrum(spectrum, spectrum_module);
		SwitchSqOfSpectrum(spectrum_module);

		gc2.GradientCell(spectrum_module);
		gc2.MyPaint();
	}
	else
	{
		spectrum.clear();
		spectrum.resize(N);
		for (int i = 0; i < N; i++)
		{
			spectrum[i].resize(N);
			for (int j = 0; j < N; j++)
			{
				complex<double> tmp;
				tmp = interpolated_picture[i][j];
				spectrum[i][j] = tmp;
			}
		}
		Fourea_2D(spectrum, -1);

		vector<vector<double>> spectrum_module;

		GetModuleSpectrum(spectrum, spectrum_module);
		SwitchSqOfSpectrum(spectrum_module);

		gc2.GradientCell(spectrum_module);
		gc2.MyPaint();

	}

	UpdateData(FALSE);
}


void CNoiseCleanerDlg::OnBnClickedClearSp()
{
	UpdateData(TRUE);
	ClearSpectrum(spectrum, energyThreshold, spectrumPic);
	gc2.GradientCell(spectrumPic);
	gc2.MyPaint();
	UpdateData(FALSE);
}


void CNoiseCleanerDlg::OnBnClickedRecoverBtn()
{
	UpdateData(TRUE);
	Fourea_2D(spectrum, 1);
	recovered_gauss.resize(N);
	for (int i = 0; i < N; i++)
	{
		recovered_gauss[i].resize(N);
		for (int j = 0; j < N; j++)
		{
			recovered_gauss[i][j] = spectrum[i][j].real();
		}
	}

	if (isBMP)
	{
		diff_input_recovered = CalculateDifference(recovered_gauss, interpolated_picture);
	} else 
		diff_input_recovered = CalculateDifference(recovered_gauss, interpolated_gauss);


	gc3.GradientCell(recovered_gauss);
	gc3.MyPaint();
	UpdateData(FALSE);
}


void CNoiseCleanerDlg::OnBnClickedImage()
{
	UpdateData(TRUE);
	int width(0), height(0);
	if(img_path == "")
		MessageBoxA(NULL, "Please, load image", NULL, MB_OK);
	else
	{
		Gdiplus::Bitmap bmp(img_path);
		image = bmp.Clone(0, 0, bmp.GetWidth(), bmp.GetHeight(), bmp.GetPixelFormat());
		width = bmp.GetWidth(),
		height = bmp.GetHeight();
		isBMP = true;

		if (width != height)
			MessageBoxA(NULL, "Image should be squred (N x N)", NULL, MB_OK);
		else
		{
			N = width;
			picture.resize(N);
			for (int x = 0; x < height; ++x)
			{
				picture[x].resize(N);
				for (int y = 0; y < width; ++y)
				{
					Color color;
					bmp.GetPixel(x, y, &color);
					picture[x][y] = (color.GetR() + color.GetG() + color.GetB()) / 3;
				}
			}

			N = Bilinear_interpolation(picture, interpolated_picture);

			AddNoise(d, interpolated_picture, interpolated_noised_picture);
			diff_noised_input = CalculateDifference(interpolated_noised_picture, interpolated_picture);

			if (d == 0) gc1.GradientCell(interpolated_picture);
			else gc1.GradientCell(interpolated_noised_picture);

			gc1.MyPaint();
		}
	}
	UpdateData(FALSE);
}


void CNoiseCleanerDlg::OnBnClickedSearch()
{
	CFileDialog dlg(TRUE);
	int result = dlg.DoModal();
	if (result == IDOK)
	{
		img_path = dlg.GetPathName();
		UpdateData(FALSE);
	}
}
