#include "pch.h"
#include "NoiseCleaner.h"
#include "NoiseCleanerDlg.h"

using namespace Gdiplus;

IMPLEMENT_DYNAMIC(GDIClass, CStatic)
ULONG_PTR gdiPlusToken;

GDIClass::GDIClass()
{
	GdiplusStartupInput gdiPlusStartapInput;
	if (GdiplusStartup(&gdiPlusToken, &gdiPlusStartapInput, NULL) != Ok)
	{
		MessageBox(L"Init error", L"Error Message", MB_OK);
		exit(0);
	}
}
GDIClass::~GDIClass()
{
	GdiplusShutdown(gdiPlusToken);
}

BEGIN_MESSAGE_MAP(GDIClass, CStatic)
END_MESSAGE_MAP()

void GDIClass::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	 Graphics g(lpDrawItemStruct->hDC);
	 Bitmap Map(lpDrawItemStruct->rcItem.right - lpDrawItemStruct->rcItem.left + 1,
		 lpDrawItemStruct->rcItem.bottom - lpDrawItemStruct->rcItem.top + 1,
		 &g);
	 Graphics g2(&Map);
	 g2.SetSmoothingMode(SmoothingModeHighSpeed);
	 Color A((BYTE)0, (BYTE)0, (BYTE)0);
	 g2.Clear(A);
	 Matrix matr;
	 g2.ResetTransform();

	 Pen p(Color::Blue, 2);
	 Pen pred(Color::Red, 1);
	 Pen psetka(Color::LightGray, 0.001);
	 SolidBrush kist(Color::White);
	 Gdiplus::REAL kx = (lpDrawItemStruct->rcItem.right - lpDrawItemStruct->rcItem.left) / (double)(xmax - xmin); 
	 Gdiplus::REAL ky = (lpDrawItemStruct->rcItem.bottom - lpDrawItemStruct->rcItem.top) / (double)(ymax - ymin);
	 matr.Scale(kx, ky);
	 g2.SetTransform(&matr);
	 if (massPic.size() != 0)
	 {
		 for (int i = 0; i < massPic.size(); i++)
		 {
			 for (int j = 0; j < massPic[i].size(); j++)
			 {
				 SolidBrush br(Color(massPic[i][j].R, massPic[i][j].G, massPic[i][j].B));
				 g2.FillRectangle(&br, i, j, 1., 1.);
			 }
		 }
	 }
	 Rect  wndRect(
		 0,
		 0,
		 lpDrawItemStruct->rcItem.right - lpDrawItemStruct->rcItem.left + 1,
		 lpDrawItemStruct->rcItem.bottom - lpDrawItemStruct->rcItem.top + 1);
	 if (im != nullptr)
	 {
		 g.DrawImage(im, wndRect);
		 //im = nullptr;
	 } else g.DrawImage(&Map, 0, 0);
}