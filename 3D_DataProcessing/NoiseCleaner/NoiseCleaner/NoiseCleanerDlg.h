// NoiseCleanerDlg.h : header file
//

#include "GDIClass.h"
#include <gl/GL.h>
#include <complex>


#pragma once

// CNoiseCleanerDlg dialog
class CNoiseCleanerDlg : public CDialogEx
{
	// Construction
public:
	CNoiseCleanerDlg(CWnd* pParent = nullptr);	// standard constructor

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_NOISECLEANER_DIALOG };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	//variabless
	GDIClass gc1;
	GDIClass gc2;
	GDIClass gc3;
	double ampl1;
	double ampl2;
	double ampl3;
	double ampl4;
	double ampl5;

	double sigma1_x;
	double sigma2_x;
	double sigma3_x;
	double sigma4_x;
	double sigma5_x;

	double sigma1_y;
	double sigma2_y;
	double sigma3_y;
	double sigma4_y;
	double sigma5_y;

	double x1;
	double x2;
	double x3;
	double x4;
	double x5;

	double y1;
	double y2;
	double y3;
	double y4;
	double y5;

	double diff_input_recovered;
	double diff_noised_input;

	size_t N;
	int d;  //percent of energy (%)
	Bitmap* image;
	int energyThreshold;
	bool isBMP;

	//vectors and structs
	vector<vector<double>> input_gauss;
	vector<vector<double>> interpolated_gauss;
	vector<vector<double>> interpolated_noised_gauss;
	vector<vector<double>> recovered_gauss;
	vector<vector<complex<double>>> spectrum;
	vector<vector<double>> spectrumPic;

	vector<vector<double>> picture;
	vector<vector<double>> interpolated_picture;
	vector<vector<double>> interpolated_noised_picture;

	//functions
	double GetGauss3D(double ampl, int i, int j, double x0, double y0, double sigma_x, double sigma_y);

	void Fourea(vector <complex<double>>& data, int is);
	void Fourea_2D(vector<vector<complex<double>>>& data, int is);

	void Linear_interpolation(vector<double> input, vector<double>& output);
	int Bilinear_interpolation(vector<vector<double>> input, vector<vector<double>>& output);

	void Transpose_2Dmatrix(vector<vector<complex<double>>> input, vector<vector<complex<double>>>& output);
	void Transpose_2Dmatrix(vector<vector<double>> input, vector<vector<double>>& output);

	void GetModuleSpectrum(vector<vector<complex<double>>> input, vector<vector<double>>& output);
	void SwitchSqOfSpectrum(vector<vector<double>>& sp);
	//clearing spectrum while energy reached threshold
	void ClearSpectrum(vector<vector<complex<double>>> &sp, int threshold, vector<vector<double>> &output);
	double GetEnergy(vector<vector<double>> v);
	double GetEnergy(vector<vector<complex<double>>> v);
	void AddNoise(int d, vector<vector<double>> input, vector<vector<double>> &output);
	void clearAllVector();
	double CalculateDifference(vector<vector<double>> signal1, vector<vector<double>> signal2);

	int Step2(int sizein);
	//buttons
	afx_msg void OnBnClickedBuildBtn();
	afx_msg void OnBnClickedFourie();

	afx_msg void OnBnClickedClearSp();
	afx_msg void OnBnClickedRecoverBtn();
	afx_msg void OnBnClickedImage();

	CButton isLogScale;
	afx_msg void OnBnClickedSearch();
	CString img_path;
};
